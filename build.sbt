name := "bookstore-assignment"

version := "0.1"

scalaVersion := "2.11.12"
// https://mvnrepository.com/artifact/com.google.code.gson/gson
libraryDependencies += "com.google.code.gson" % "gson" % "1.7.1"
libraryDependencies += "com.rabbitmq" % "amqp-client" % "5.6.0"
libraryDependencies += "org.slf4j" % "slf4j-api" % "1.7.13"
libraryDependencies += "org.slf4j" % "slf4j-log4j12" % "1.7.13"