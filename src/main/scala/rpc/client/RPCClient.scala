package rpc.client

import java.util.UUID

import com.rabbitmq.client.AMQP.BasicProperties
import com.rabbitmq.client.{Channel, Connection, ConnectionFactory}
import models.{Book, SearchQuery}
import rpc.CancelCall
import utils.JsonParser

import scala.util.{Failure, Success, Try}

class  RPCClient(host: String) {
  val factory = new ConnectionFactory()
  factory.setHost(host)

  val connection: Connection = factory.newConnection()
  val channel: Channel = connection.createChannel()

  val requestQueueName: String = "rpc_queue"
  val replyQueueName: String = channel.queueDeclare().getQueue

  def call( message: String ,operation:String): String  = {
    val corrId  = UUID.randomUUID().toString
    val  props = new BasicProperties.Builder().correlationId(corrId)
      .`type`(operation)
      .replyTo(replyQueueName)
      .build()

    channel.basicPublish("", requestQueueName, props, message.getBytes("UTF-8"))

    val  responseCallback = new ResponseCallback(corrId)
    val cancelCallback = new CancelCall
    channel.basicConsume(replyQueueName, true, responseCallback, cancelCallback)
    responseCallback.take()
  }
  def close (): Unit = {
    connection.close()
  }
}

object RPCClient {

  def main(args: Array[String]): Unit = {
    var bookStoreRPC: RPCClient = null
    var response: String = null
    var operation: String = null
    var  contentString :String = null
    val host = "localhost"

    Try(new RPCClient(host)) match {
      case Success(value) =>
        bookStoreRPC = value

        //     Test case for Search by Author
//        operation = "SEARCH"
//        val messageContent = SearchQuery("author", "Dan Brown")
//        contentString = JsonParser.convertSearchQueryToJson(messageContent)

        //  Test case for search by isbn
        operation = "SEARCH"
        val messageContent = SearchQuery("isbn", "1110")
        contentString = JsonParser.convertSearchQueryToJson(messageContent)

          //  Test case for search by title
//        operation = "SEARCH"
//        val messageContent = SearchQuery("title", "Da Vinci Code")
//        contentString = JsonParser.convertSearchQueryToJson(messageContent)

        // Test case for Adding a new book
//        operation ="ADD"
//        val  messageContent = Book("1120", "Charlie and the Chocolate Factory", "Roal Dahl", 25.78, "Fiction")
//        contentString = JsonParser.convertBookToJson(messageContent)

        // Test case for Getting all books
//        operation = "GET"
//        contentString =""


        println(" [x] Requesting: "  + operation + contentString)
        response = bookStoreRPC.call(contentString,operation)
        println(" [.] Got: " + response)

      case Failure(exception) =>
        println("Error in Connecting to RabbitMQ:\n" + exception )
    }

    Try (bookStoreRPC.close()) match {
      case Success(value) =>
        println("Disconnected from RabbitMQ")
      case Failure(exception) =>
        println("Error in disconnecting from Rabbit MQ:\n" + exception )
    }
  }
}

