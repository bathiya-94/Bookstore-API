package rpc.client

import java.util.concurrent.{ArrayBlockingQueue, BlockingQueue}

import com.rabbitmq.client.{DeliverCallback, Delivery}

class  ResponseCallback(val corrId: String) extends DeliverCallback {

  val response: BlockingQueue[String] = new  ArrayBlockingQueue[String](1)

  override def handle(consumerTag: String, message: Delivery): Unit = {
    if (message.getProperties.getCorrelationId.equals(corrId)) {
      response.offer(new String(message.getBody, "UTF-8"))
    }
  }

  def take(): String = {
    response.take()
  }
}