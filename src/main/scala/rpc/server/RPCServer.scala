package rpc.server

import java.util.concurrent.CountDownLatch

import com.rabbitmq.client.{Channel, Connection, ConnectionFactory}
import rpc.CancelCall

import scala.util.{Failure, Success, Try}

object RPCServer {

  private val RPC_QUEUE_NAME = "rpc_queue"

  def main(args: Array[String]): Unit = {
    var connection: Connection = null
    var channel: Channel = null

    Try(new ConnectionFactory()) match {
      case Success(value) =>
        val factory = value
        factory.setHost("localhost")

        connection = factory.newConnection()
        channel = connection.createChannel()

        channel.queueDeclare(RPC_QUEUE_NAME, false, false, false, null)
        channel.basicQos(1)

        val latch = new CountDownLatch(1)

        Try(new ServerCallback(channel,latch)) match {
          case Success(value) =>
            val serverCallback =value
            val cancelCallback = new CancelCall

            channel.basicConsume(RPC_QUEUE_NAME, false, serverCallback, cancelCallback)
            println(" [x] Awaiting RPC request")
            latch.await()
          case Failure(exception) =>
            println("RabbitMQ Server Callback Error: " +exception)
        }
      case Failure(exception) =>
        println("RabbitMQ Creating Connection Error:\n" + exception)
    }

    Try(connection.close()) match {
      case Success(value) =>
        println("Connection closed successfully")
      case Failure(exception) =>
        println("RabbitMQ disconnecting error: \n" + exception)
    }
  }
}
