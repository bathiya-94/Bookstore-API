package rpc.server

import java.util.concurrent.CountDownLatch

import com.rabbitmq.client.AMQP.BasicProperties
import com.rabbitmq.client.{Channel, DeliverCallback, Delivery}
import services.BookService
import utils.{JsonParser, StringJoiner}

class ServerCallback(val ch: Channel, val latch: CountDownLatch) extends DeliverCallback{
  override def handle(consumerTag: String, delivery: Delivery): Unit = {
    var response: String = null
    val replyProps = new BasicProperties.Builder()
      .correlationId(delivery.getProperties.getCorrelationId)
      .build

    val operation = new String(delivery.getProperties.getType)
    val  message = new String(delivery.getBody, "UTF-8")
    val messages = message.split(" ")

    operation match {
      case "GET" =>
        response =  JsonParser.convertListToJson(BookService.getAllBooks)

      case "SEARCH" =>
        val searchQuery = JsonParser.convertJsonToSearchQuery(message)
        val searchTerm = searchQuery.searchTerm
        val value  = searchQuery.searchValue
        if (searchTerm =="isbn")
          response = JsonParser.convertBookToJson(BookService.getBookByISBN(value))
        else
          response = JsonParser.convertListToJson(BookService.getBooksBySearchTerm(searchTerm,value))

      case "ADD"=>
        val newBook = JsonParser.convertJsonToBook(message)
        response = JsonParser.convertBookToJson(BookService.postBook(newBook))

      case _ =>
        response = "Invalid operation"
      }
      println(" [.] " + response)
      ch.basicPublish("", delivery.getProperties.getReplyTo,
        replyProps, response.getBytes("UTF-8"))
      ch.basicAck(delivery.getEnvelope.getDeliveryTag,false)
      latch.countDown()
  }
}
