package utils

import scala.collection.JavaConverters._
import com.google.gson.Gson
import models.{Book, SearchQuery}

object JsonParser {

  val gson = new Gson()

  def convertListToJson (list: List[Book]) :String = {
    val listJ = list.asJava
    gson.toJson(listJ)
  }

  def convertBookToJson(book:Book) :String ={
    gson.toJson(book)
  }

  def convertSearchQueryToJson(searchQuery:SearchQuery):String = {
    gson.toJson(searchQuery)
  }

  def convertBookToJson(optionBook:Option[Book]) :String ={
    optionBook match {
      case Some(book) => gson.toJson(book)
      case None => gson.toJson(None)
    }
  }

  def convertJsonToBook(jsonString : String) :Book= {
    gson.fromJson(jsonString, classOf[Book])
  }

  def convertJsonToSearchQuery(jsonString: String):SearchQuery = {
    gson.fromJson(jsonString,classOf[SearchQuery])
  }

  def convertJsonToBookList(jsonString : String) : List[Book] = {
    gson.fromJson(jsonString,classOf[List[Book]])
  }


}
