package services

import models.Book

import scala.collection.mutable

object BookService {

  val book1  = new  Book ("1110",
    "Da Vinci Code",
    "Dan Brown",
    25.5 ,
   "fiction")

  val book2  = new  Book ("1111",
    "Inferno",
    "Dan Brown",
    25.87 ,
    "fiction")

  val book3  = new  Book ("1113",
    "Black Market",
    "James Patterson",
    28.87 ,
    "fiction")

  val bookStore: mutable.Map[String, Book] = mutable.Map(book1.isbn -> book1, book2.isbn -> book2, book3.isbn -> book3)

  def getBookByISBN(isbnNo :String): Option[Book]= {
    bookStore.get(isbnNo)
  }

  def getAllBooks: List[Book] = {
    bookStore.values.toList
  }

  def getBooksBySearchTerm (query:String,value:String): List[Book]= {
    query match {
      case "title" =>
        bookStore.values.filter(Book => Book.title == value).toList
      case "author" =>
        bookStore.values.filter(Book => Book.author == value).toList
      case "category" =>
        bookStore.values.filter(Book => Book.category == value).toList
      case _ => List()
    }
  }

  def postBook(newBook : Book) : Book = {
    bookStore += (newBook.isbn -> newBook)
    newBook
  }

}
