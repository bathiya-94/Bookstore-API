package models

case class SearchQuery(
                        var searchTerm: String,
                        var searchValue: String
                        )
{
}