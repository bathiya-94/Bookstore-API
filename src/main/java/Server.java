import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import services.BookService;
import utils.JsonParser;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Server {

    private final static Logger LOGGER =
            Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    private final static  String RESPONSE_TYPE_JSON = "application/json;charset=utf-8";
    private final static  String RESPONSE_TYPE_TEXT = "text/plain";

    public static void main(String[] args) throws Exception {

        HttpServer server = HttpServer.create(new InetSocketAddress(8000), 0);
//        server.createContext("/", new MyHandler());
        server.createContext("/books", new BooksHandler());
        server.createContext("/books/book", new BookHandler());
        server.setExecutor(null); // creates a default executor
        server.start();
        LOGGER.log(Level.INFO,"Server started on port 8000");
    }

    static class BookHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange httpExchange) throws IOException {

            logRequestInfo(httpExchange);

            if (httpExchange.getRequestMethod().equals("GET"))
            {
                String path = httpExchange.getRequestURI().getPath();
                String[] splitPaths = path.split("/");

                if (splitPaths.length == 4) {
                    String response =JsonParser.convertBookToJson(BookService.getBookByISBN(splitPaths[3]));
                    Server.writeResponse(httpExchange,200,RESPONSE_TYPE_JSON,response);
                }
                else {
                    Server.writeResponse(httpExchange,404,RESPONSE_TYPE_TEXT,"Invalid URI");
                }
            }
            else {
                StringBuilder sb = new StringBuilder();
                InputStream ios = httpExchange.getRequestBody();
                int i;
                while ((i = ios.read()) != -1) {
                    sb.append((char) i);
                }
                String newBook = JsonParser.convertBookToJson(BookService.postBook(
                        JsonParser.convertJsonToBook(sb.toString())));
                Server.writeResponse(httpExchange,400,RESPONSE_TYPE_JSON,newBook);
            }

        }
    }


    static  class  BooksHandler implements  HttpHandler {
        @Override
        public void handle(HttpExchange httpExchange) throws IOException {

            logRequestInfo(httpExchange);

            String query  =httpExchange.getRequestURI().getQuery();
            if (query == null) {
                Server.writeResponse(httpExchange,200,RESPONSE_TYPE_JSON,
                        JsonParser.convertListToJson(BookService.getAllBooks()));
            }
            else {
                String[] splitQuery = query.split("=");
                    Server.writeResponse(httpExchange,200,RESPONSE_TYPE_JSON,
                            JsonParser.convertListToJson(BookService
                                    .getBooksBySearchTerm(splitQuery[0],splitQuery[1])));
            }
        }
    }

    public static void writeResponse(HttpExchange httpExchange,int responseCode, String responseType,
                                     String response) throws IOException {
        final Headers headers = httpExchange.getResponseHeaders();
        headers.add("Content-Type",responseType);

        httpExchange.sendResponseHeaders(responseCode, response.length());
        OutputStream os = httpExchange.getResponseBody();
        os.write(response.getBytes());
        os.close();
        LOGGER.log(Level.INFO,response);
    }

    public static  void logRequestInfo (HttpExchange httpExchange){
        String  requestMethod= httpExchange.getRequestMethod();
        String uri = httpExchange.getRequestURI().toString();
        LOGGER.log(Level.INFO,requestMethod + uri);
    }


}
